<?php

namespace Drupal\shib_auth\Controller;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Handles routes for the shib auth module.
 */
class ShibController extends ControllerBase {

  /**
   * Lists all shib auth rules with edit,delete, and clone links.
   *
   * @return array
   *   HTML table containing the number of rule, attribute, RegExp,
   *   role and the actions which can be done with each role.
   */
  public function shibAuthListRules() {
    $rules = shib_auth_get_rules();

    $roles_map = shib_auth_get_role_options();

    $output = [];
    $rows = [];

    foreach ($rules as $rule) {
      $roles_list = [];

      foreach ($rule['role'] as $role) {
        if (!empty($role)) {
          $roles_list[] = $roles_map[$role];
        }
      }

      $clone_link = Link::fromTextAndUrl('Clone', Url::fromRoute('shib_auth.clone_rule', ['id' => $rule['id']]))->toString();
      $edit_link = Link::fromTextAndUrl('Edit', Url::fromRoute('shib_auth.edit_rule', ['id' => $rule['id']]))->toString();
      $delete_link = Link::fromTextAndUrl('Delete', Url::fromRoute('shib_auth.delete_rule', ['id' => $rule['id']]))->toString();

      $action_links = t('@clone_link | @edit_link | @delete_link',
        [
          '@clone_link' => $clone_link,
          '@edit_link' => $edit_link,
          '@delete_link' => $delete_link,
        ]
      );

      $rows[] = [
        $rule['field'],
        urldecode($rule['regexpression']),
        implode(',', $roles_list),
        $action_links,
      ];
    }

    $output['rules'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Attribute'),
        $this->t('RegExp'),
        $this->t('Roles'),
        $this->t('Actions'),
      ],
      '#rows' => $rows,
    ];

    $link = Link::fromTextAndUrl('Add new rule', Url::fromRoute('shib_auth.new_rule'))->toString();
    $output['new_rule'] = [
      '#type' => 'markup',
      '#markup' => $link,
    ];

    return $output;
  }

  /**
   * Display the markup.
   *
   * This function prevents drupal loading a cached page after shibboleth login.
   *
   * @return object
   *   Redirect Response.
   */
  public function shibAuthLogin() {
    $path = \Drupal::request()->get('path');
    
    switch ($path) {
      case '/user/login':
        $redirect_url = \Drupal::request()->getSchemeAndHttpHost() . '/user';
      break;

      case '/shib_link':
        $redirect_url = \Drupal::request()->getSchemeAndHttpHost() . '/user';
      break;

      default:
        $redirect_url = \Drupal::request()->getSchemeAndHttpHost() . $path;
      break;
    }

    return new RedirectResponse($redirect_url);
  }

  /**
   * This function manages account linking.
   */
  public function shibAuthAccountLink() {
    $_SESSION['shib_auth_account_linking'] = TRUE;
    return new RedirectResponse(shib_auth_generate_login_url());
  }

}

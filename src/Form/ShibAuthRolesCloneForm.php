<?php

namespace Drupal\shib_auth\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Confirmation form for cloning a shib auth rule.
 */
class ShibAuthRolesCloneForm extends ConfirmFormBase {

  /**
   * The ID of the item to clone.
   *
   * @var string
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'shib_auth_role_clone';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('The rule with id @id will be cloned.', ['@id' => $this->id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('shib_auth.rule');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Confirm');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    shib_auth_clone_rule($this->id);
    $url = Url::fromRoute('shib_auth.rule');
    $form_state->setRedirectUrl($url);
  }

}

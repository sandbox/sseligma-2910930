<?php

namespace Drupal\shib_auth\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;

/**
 * Configure example settings for this site.
 */
class ShibAuthRolesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'shib_auth_admin_rules';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'shib_auth.general',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {

    if (isset($id)) {
      $rules = shib_auth_get_rules($id);
    }

    $form['id'] = [
      '#title' => t('Entry id'),
      '#type' => 'hidden',
      '#default_value' => isset($rules[0]) ? $rules[0]['id'] : NULL,
    ];

    $form['field'] = [
      '#title' => t('Shibboleth attribute name'),
      '#type' => 'textfield',
      '#default_value' => isset($rules[0]) ? $rules[0]['field'] : '',
      '#require' => TRUE,
      '#description' => t("More properly: <b>@server</b> field name; enable DEBUG mode to list available fields. <br/>Note that it might differ from your users' fields.", ['@server' => '$_SERVER']),
    ];

    $form['regexpression'] = [
      '#title' => t('Value (regexp)'),
      '#type' => 'textfield',
      '#default_value' => isset($rules[0]) ? $rules[0]['regexpression'] : '',
      '#require' => TRUE,
    ];

    $form['role'] = [
      '#type' => 'checkboxes',
      '#title' => t('Roles'),
      '#default_value' => isset($rules[0]) ? $rules[0]['role'] : [],
      '#options' => shib_auth_get_role_options(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $conn = Database::getConnection();

    $data = [
      'id' => (int) $form_state->getValue('id'),
      'field' => $form_state->getValue('field'),
      'regexpression' => $form_state->getValue('regexpression'),
      'role' => $form_state->getValue('role'),
    ];

    shib_auth_save_rule($data);

    $url = Url::fromRoute('shib_auth.rule');
    $form_state->setRedirectUrl($url);
  }

}

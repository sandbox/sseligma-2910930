<?php

namespace Drupal\shib_auth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ShibAuthConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'shib_auth_admin_general';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'shib_auth.general',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('shib_auth.settings');

    $form['shib_handler_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Shibboleth handler settings'),
      '#weight' => -10,
      '#collapsible' => FALSE,
    ];

    $form['shib_attribute_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Attribute settings'),
      '#weight' => -5,
      '#collapsible' => FALSE,
    ];

    $form['shib_handler_settings']['full_handler_url'] = [
      '#type' => 'textfield',
      '#title' => t('Shibboleth login handler URL'),
      '#default_value' => $config->get('full_handler_url'),
      '#description' => t('The URL can be absolute or relative to the server base url: http://www.example.com/Shibboleth.sso/DS; /Shibboleth.sso/DS'),
    ];

    $form['shib_handler_settings']['full_logout_url'] = [
      '#type' => 'textfield',
      '#title' => t('Shibboleth logout handler URL'),
      '#default_value' => $config->get('full_logout_url'),
      '#description' => t('The URL can be absolute or relative to the server base url: http://www.example.com/Shibboleth.sso/Logout; /Shibboleth.sso/Logout'),
    ];

    $form['shib_handler_settings']['link_text'] = [
      '#type' => 'textfield',
      '#title' => t('Shibboleth login link text'),
      '#default_value' => $config->get('link_text'),
      '#description' => t('The text of the login link. You can change this text on the Shibbolet login block settings form too!'),
    ];

    $form['shib_handler_settings']['force_https'] = [
      '#type' => 'checkbox',
      '#title' => t('Force HTTPS on login'),
      '#description' => t('The user will be redirected to HTTPS'),
      '#default_value' => $config->get('force_https'),
    ];

    $form['shib_attribute_settings']['username_variable'] = [
      '#type' => 'textfield',
      '#title' => t('Server variable for username'),
      '#default_value' => $config->get('username_variable'),
    ];

    $form['shib_attribute_settings']['email_variable'] = [
      '#type' => 'textfield',
      '#title' => t('Server variable for e-mail address'),
      '#default_value' => $config->get('email_variable'),
    ];

    $form['shib_attribute_settings']['account_linking'] = [
      '#type' => 'checkbox',
      '#title' => t('Account linking'),
      '#description' => t('Allow locally authenticated users to link their Drupal accounts to federated logins. Note that disabling this option only prevents from creating/removing associations, existing links will remain valid.'),
      '#default_value' => $config->get('account_linking'),
    ];

    $form['shib_attribute_settings']['account_linking_text'] = [
      '#type' => 'textfield',
      '#title' => t('Shibboleth account linking text'),
      '#default_value' => $config->get('account_linking_text'),
      '#description' => t('The text of the link providing account linking shown on the user settings form.'),
    ];

    $form['shib_attribute_debug'] = [
      '#type' => 'fieldset',
      '#title' => 'Debugging options',
      '#weight' => -1,
    ];

    $form['shib_attribute_debug']['debug_state'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable DEBUG mode.'),
      '#default_value' => $config->get('debug_state'),
    ];

    $form['shib_attribute_debug']['debug_url'] = [
      '#type' => 'textfield',
      '#title' => t('DEBUG path prefix'),
      '#default_value' => $config->get('debug_url'),
      '#description' => t("For example use 'user' for display DEBUG messages on paths 'user/*'!"),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('shib_auth.settings')
      ->set('full_handler_url', $form_state->getValue('full_handler_url'))
      ->set('full_logout_url', $form_state->getValue('full_logout_url'))
      ->set('link_text', $form_state->getValue('link_text'))
      ->set('force_https', $form_state->getValue('force_https'))
      ->set('username_variable', $form_state->getValue('username_variable'))
      ->set('email_variable', $form_state->getValue('email_variable'))
      ->set('account_linking', $form_state->getValue('account_linking'))
      ->set('account_linking_text', $form_state->getValue('account_linking_text'))
      ->set('debug_state', $form_state->getValue('debug_state'))
      ->set('debug_url', $form_state->getValue('debug_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\shib_auth\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to shib auth events.
 */
class ShibauthSubscriber implements EventSubscriberInterface {

  /**
   * Handle onRequest event.
   */
  public function onRequest(GetResponseEvent $event) {
    \Drupal::logger('shib_auth')->notice('onRequest ' . microtime());

    $current_path = \Drupal::service('path.current')->getPath();
    shib_auth_init();
  }

  /**
   * Handle onResponse event.
   */
  public function onResponse() {
    global $_shib_auth_logout;

    \Drupal::logger('shib_auth')->notice('onTerminate ' . microtime());
    \Drupal::logger('shib_auth')->notice('in terminate shib_auth_authentication = ' . $_SESSION['shib_auth_authentication']);
    \Drupal::logger('shib_auth')->notice('in terminate _shib_auth_logout = ' . $_shib_auth_logout);

    if (isset($_shib_auth_logout) && $_shib_auth_logout === TRUE) {
      $_shib_auth_logout = FALSE;
      unset($_SESSION['shib_auth_rolelog']);
      unset($_SESSION['shib_auth_authentication']);
      $logouthandler = shib_auth_get_redirect_base(shib_auth_config('full_logout_url'));
      $logout_redirect = shib_auth_config('logout_url');
      $logout_url = check_url($logouthandler . '?return=' . urlencode($logout_redirect));

      $response = new RedirectResponse($logout_url);
      $response->send();
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest'];
    $events[KernelEvents::RESPONSE][] = ['onResponse'];
    return $events;
  }

}

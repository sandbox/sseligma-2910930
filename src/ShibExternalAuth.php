<?php

namespace Drupal\shib_auth;

use Drupal\externalauth\ExternalAuth;

/**
 * This ExternalAuth subclass adds shib_auth specific features.
 */
class ShibExternalAuth extends ExternalAuth {

  /**
   * {@inheritdoc}
   */
  public function login($authname, $provider) {
    if ($result = parent::login($authname, $provider)) {
      $_SESSION['shib_auth_authentication'] = 'shib_auth';
      return $result;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function loginRegister($authname, $provider, $account_data = [], $authmap_data = NULL) {
    if ($result = parent::loginRegister($authname, $provider, $account_data, $authmap_data)) {
      return $result;
    }

    return FALSE;
  }

}

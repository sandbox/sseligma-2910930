<?php

namespace Drupal\shib_auth\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Shib' Block.
 *
 * @Block(
 *   id = "shib_auth_login_box_block",
 *   admin_label = @Translation("Shib Auth Login Box block"),
 * )
 */
class ShibAuthLoginBoxBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => login_url_html(),
    ];
  }

}
